from flask import Flask, request, send_from_directory
from flask.ext.cors import CORS
from phue import Bridge
import random
import sys
import copy
import time

app = Flask(__name__)

b = Bridge('1.1.1.10') # Veranderen naar het adres van de Bridge
b.connect()

class Game:

  def __init__(self):
    self.board = [' '] * 9
    self.player1_name = ''
    self.player1_marker = 'X'
    self.player1_hue = 47000
    self.player2_name = ''
    self.player2_marker = 'O'
    self.player2_hue = 0
    self.is_running = False
    self.lights = [1,2,3,4,5,6,7,8,9]
    self.winning_combos = (
      [6, 7, 8], [3, 4, 5], [0, 1, 2], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6],
    )
    self.corners = [0,2,6,8]
    self.sides = [1,3,5,7]
    self.middle = 4
    self.count = 0

    self.form = '''
      \t| %s | %s | %s |
      \t-------------
      \t| %s | %s | %s |
      \t-------------
      \t| %s | %s | %s |
      '''                  

  def lamp_on(self, lamp, hue):
    b.set_light(lamp, 'on', True)
    b.set_light(lamp, 'hue', hue)
    b.set_light(lamp, 'sat', 254)
    b.set_light(lamp, 'bri', 254)

  def rainbow(self):
    count = 0
    color = 0
    while (count < 7):
      b.set_light(self.lights, 'on', True)
      b.set_light(self.lights, 'bri', 254)
      b.set_light(self.lights, 'sat', 254)
      b.set_light(self.lights, 'hue', color, transitiontime = 5)
      if color != 50000:
          color += 10000
      else:
          color = 0
      count += 1
      time.sleep(0.5)

  def win(self, hue):
    x = 0
    b.set_light(self.lights, 'on', True)
    b.set_light(self.lights, 'hue', hue)
    b.set_light(self.lights, 'sat', 254)
    b.set_light(self.lights, 'bri', 254)
    while(x < 3):
      time.sleep(1)
      b.set_light(self.lights, 'on', False)
      time.sleep(1)
      b.set_light(self.lights, 'on', True)
      x += 1

  def draw(self):
    b.set_light(self.lights, 'on', True)
    b.set_light(self.lights, 'sat', 254)
    b.set_light(self.lights, 'bri', 254)
    x = 0
    while(x < 3):
      b.set_light([1,2,3,4,6,7,8,9], 'hue', self.player1_hue)
      b.set_light(5, 'hue', self.player2_hue)
      time.sleep(1)
      b.set_light([1,2,3,4,6,7,8,9], 'hue', self.player2_hue)
      b.set_light(5, 'hue', self.player1_hue)
      time.sleep(1)
      x += 1
    b.set_light(self.lights, 'on', False)

  def print_light_board(self):

    if self.board[0] == self.player1_marker:
      self.lamp_on(1, self.player1_hue)
    elif self.board[0] == self.player2_marker:
      self.lamp_on(1, self.player2_hue)
    else:
      b.set_light(1, 'on', False)
    
    if self.board[1] == self.player1_marker:
      self.lamp_on(2, self.player1_hue)
    elif self.board[1] == self.player2_marker:
      self.lamp_on(2, self.player2_hue)
    else:
      b.set_light(2, 'on', False)
    
    if self.board[2] == self.player1_marker:
      self.lamp_on(3, self.player1_hue)
    elif self.board[2] == self.player2_marker:
      self.lamp_on(3, self.player2_hue)
    else:
      b.set_light(3, 'on', False)
    
    if self.board[3] == self.player1_marker:
      self.lamp_on(4, self.player1_hue)
    elif self.board[3] == self.player2_marker:
      self.lamp_on(4, self.player2_hue)
    else:
      b.set_light(4, 'on', False)
    
    if self.board[4] == self.player1_marker:
      self.lamp_on(5, self.player1_hue)
    elif self.board[4] == self.player2_marker:
      self.lamp_on(5, self.player2_hue)
    else:
      b.set_light(5, 'on', False)
    
    if self.board[5] == self.player1_marker:
      self.lamp_on(6, self.player1_hue)
    elif self.board[5] == self.player2_marker:
      self.lamp_on(6, self.player2_hue)
    else:
      b.set_light(6, 'on', False)

    if self.board[6] == self.player1_marker:
      self.lamp_on(7, self.player1_hue)
    elif self.board[6] == self.player2_marker:
      self.lamp_on(7, self.player2_hue)
    else:
      b.set_light(7, 'on', False)

    if self.board[7] == self.player1_marker:
      self.lamp_on(8, self.player1_hue)
    elif self.board[7] == self.player2_marker:
      self.lamp_on(8, self.player2_hue)
    else:
      b.set_light(8, 'on', False)

    if self.board[8] == self.player1_marker:
      self.lamp_on(9, self.player1_hue)
    elif self.board[8] == self.player2_marker:
      self.lamp_on(9, self.player2_hue)
    else:
      b.set_light(9, 'on', False)

  def print_board(self,board = None):
    "Display board on screen"
    if board is None:
      print self.form % tuple(self.board[6:9] + self.board[3:6] + self.board[0:3])
      self.print_light_board()
    else:
      # when the game starts, display numbers on all the grids
      print self.form % tuple(board[6:9] + board[3:6] + board[0:3])

  def is_winner(self, board, marker):
    for combo in self.winning_combos:
      if (board[combo[0]] == board[combo[1]] == board[combo[2]] == marker):
        return True
    return False

  def is_space_free(self, board, index):
    "checks for free space of the board"
    # print "SPACE %s is taken" % index
    return board[index] == ' '

  def is_board_full(self):
    for i in range(1,9):
      if self.is_space_free(self.board, i):
        return False
    return True

  def choose_random_move(self, move_list):
    possible_winning_moves = []
    for index in move_list:
      if self.is_space_free(self.board, index):
        possible_winning_moves.append(index)
        if len(possible_winning_moves) != 0:
          return random.choice(possible_winning_moves)
        else:
          return None

  def get_bot_move(self):
    for i in range(0,len(self.board)):
      board_copy = copy.deepcopy(self.board)
      if self.is_space_free(board_copy, i):
        self.make_move(board_copy,i,self.player2_marker)
        if self.is_winner(board_copy, self.player2_marker):
          return i
    for i in range(0,len(self.board)):
      board_copy = copy.deepcopy(self.board)
      if self.is_space_free(board_copy, i):
        self.make_move(board_copy,i,self.player1_marker)
        if self.is_winner(board_copy, self.player1_marker):
          return i

    move = self.choose_random_move(self.corners)
    if move != None:
      return move

    if self.is_space_free(self.board,self.middle):
      return self.middle
        
    return self.choose_random_move(self.sides)

  def make_move(self,board,index,move):
    board[index] = move

  def start_game(self):
    if (self.is_running):
      return '{"message": "Iemand is al bezig, probeer later opnieuw"}'
    else:
      b.set_light(self.lights, 'on', False)
      self.__init__()
      print "New game started"
      self.is_running = True;
      if random.randint(0,1) == 0:
        bot_move = game.get_bot_move()
        game.make_move(game.board, bot_move, 'O')
        game.print_board()
        return '{"show_board": "true", "message": "Het gebouw begint", "bot_move": "%s"}' % str(bot_move+1)
      else:
        return '{"show_board": "true", "message": "Je mag beginnen"}'

game = Game()

cors = CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/start')
def start():
  return game.start_game()

@app.route('/draw')
def draw():
  return game.draw()

@app.route('/reset')
def reset():
  game.__init__()
  return '{"message": "Reset geslaagd."}'

@app.route('/clear')
def clear():
  return '{"message": " "}'

@app.route('/win/<int:hue>')
def win(hue):
  return game.win(hue)

@app.route('/rainbow')
def rainbow():
  print "Logo click"
  game.count += 1
  print game.count
  if game.count is 5:
    game.rainbow()
  return str(game.count)


@app.route('/move/<int:pos>')
def move(pos):
  if game.is_running:
    if not game.is_space_free(game.board,pos-1):
      return '{"message": "Ongeldige zet"}'
    game.make_move(game.board, pos-1, 'X')
    game.print_board()
    if(game.is_winner(game.board, game.player1_marker)):
      game.is_running = False
      return '{"message": "Je hebt gewonnen!", "player_move": "' + str(pos) + '"}'
    elif game.is_board_full():
      game.is_running = False
      return '{"message": "Gelijkspel!", "player_move": "' + str(pos) + '"}'
    print "Made move"
    bot_move = game.get_bot_move()
    game.make_move(game.board, bot_move, 'O')
    game.print_board()
    if (game.is_winner(game.board, game.player2_marker)):
      game.is_running = False
      return '{"message": "Het gebouw wint!", "player_move": "' + str(pos) + '", "bot_move": "%s"}' % str(bot_move+1)
    elif game.is_board_full():
      game.is_running = False
      return '{"message": "Gelijkspel!", "player_move": "' + str(pos) + '", "bot_move": "%s"}' % str(bot_move+1)
    return '{"message": " ", "player_move": "' + str(pos) + '", "bot_move": "%s"}' % str(bot_move+1)
    game.print_board()

if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=True)
