# LAHG

### Frontend (Node.js vereist)
Pas het adres van de backend aan op de juiste plekken in **reset.js** en **scripts/script.js**

Run het volgende in de frontend directory om de benodigde dependencies te installeren en om op te bouwen:
```sh
$ npm install && npm build
```
Om de frontend te starten:
```sh
$ node serer.js
```
### Backend (Python vereist)
Pas het adres van de Bridge aan in **app.py**

Run app.py met python om de backend te starten:
```sh
$ python app.py
```