var express = require('express');
var app = express();

app.use(express.static('build'));


port = 80;
app.listen(port, function() {
    console.log('Running on port ' + port);
});
