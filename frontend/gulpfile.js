var gulp = require('gulp');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var htmlmin = require('gulp-htmlmin');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');

var remaining = ['source/**', '!source/scripts/*.js', '!source/*.html', '!source/stylesheets/*.scss', '!source/stylesheets/*.css', '!source/images/**/*'];

gulp.task('stylesheets', function() {
    gulp.src('source/stylesheets/main.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename('style.css'))
        .pipe(gulp.dest('build/stylesheets'))
        .pipe(connect.reload());
    gulp.src('source/stylesheets/*.css')
        .pipe(cssnano())
        .pipe(gulp.dest('build/stylesheets'))
        .pipe(connect.reload());
});

gulp.task('scripts', function() {
    gulp.src('source/scripts/*.js')
        .pipe(uglify())
        .pipe(concat('script.js'))
        .pipe(gulp.dest('build/scripts'))
        .pipe(connect.reload());
});

gulp.task('pages', function() {
    gulp.src('source/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('build'))
        .pipe(connect.reload());
});

gulp.task('images', function() {
    gulp.src('source/images/**/*')
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest('build/images/'))
        .pipe(connect.reload());
});

gulp.task('files', function() {
    gulp.src(remaining)
        .pipe(gulp.dest('build'))
        .pipe(connect.reload());
});

gulp.task('default', ['stylesheets', 'scripts', 'pages', 'images', 'files']);

gulp.task('watch', function() {
    gulp.watch('source/stylesheets/**', ['stylesheets']);
    gulp.watch('source/scripts/*.js', ['scripts']);
    gulp.watch('source/*.html', ['pages']);
    gulp.watch('source/images/**/*', ['images']);
    gulp.watch(remaining, ['files']);
});

gulp.task('connect', function() {
    connect.server({
        root: 'build',
        livereload: true,
        port: 8888
    });
});

gulp.task('start', ['connect', 'watch']);
