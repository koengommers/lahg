$(document).ready(function() {

host = 'http://1.1.1.5:5000/'; // Veranderen naar het backend adres

response = function(data) {
    data = JSON.parse(data);
    if (data.message) {
        $('.message').html(data.message);
    }
    if (data.bot_move) {
        setTimeout(function(){
            $('.box:nth-child(' + data.bot_move + ')').css('background-color', '#c0392b');
        }, 100);
    }
    if (data.player_move) {
        $('.box:nth-child(' + data.player_move + ')').css('background-color', '#3498db');
    }
    if (data.message == "Gelijkspel!") {
        $.get(host + 'draw');
        setTimeout(function(){
            $('.board').hide();
            $('.button').show();
            $('.button').html('Speel opnieuw!');
            $('.box').css('background-color', '#555555');
            $('.message').html(' ');
        }, 8000);
    }
    if (data.message == "Het gebouw wint!") {
        $.get(host + 'win/0');
        setTimeout(function(){
            $('.board').hide();
            $('.button').show();
            $('.button').html('Speel opnieuw!');
            $('.box').css('background-color', '#555555');
            $('.message').html(' ');
        }, 8000);
    }
    if (data.message == "Je hebt gewonnen!") {
        $.get(host + 'win/47000');
        setTimeout(function(){
            $('.board').hide();
            $('.button').show();
            $('.button').html('Speel opnieuw!');
            $('.box').css('background-color', '#555555');
            $('.message').html(' ');
        }, 8000);
    }
};

    $('img').click(function() {
        $.get(host + 'rainbow');
    });

    $('.button').click(function() {
        $.get(host + 'start', function(data) {
            data = JSON.parse(data);
            if (data.show_board == 'true') {
                $('.board').show();
                $('.button').hide();
            }
            if (data.message) {
                $('.message').html(data.message);
            }
            if (data.bot_move) {
                setTimeout(function(){
                    $('.box:nth-child(' + data.bot_move + ')').css('background-color', '#c0392b');
                }, 100);
            }
        });
    });


    $('.box').click(function() {
        index = $(this).index() + 1;
        $.get(host + 'move/'+index, function(data) {
            response(data);
        });
    });
});
