$(document).ready(function() {
    host = 'http://1.1.1.5:5000/'; // Veranderen naar het backend adres
    $('.button').click(function() {
        $.get(host + 'reset', function(data) {
            data = JSON.parse(data);
            $('.message').html(data.message);
            setTimeout(function() {
                $.get(host + 'clear', function(clear) {
                    clear = JSON.parse(clear);
                    $('.message').html(clear.message);
                });
            }, 2000);
        });
    });
});
